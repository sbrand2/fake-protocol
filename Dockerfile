# Use an official Python runtime as a parent image
FROM python:3.8-slim

ARG CI_COMMIT_SHORT_SHA=main

# Set the working directory to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app

# Expose Prometheus metrics port
EXPOSE 8080

ENV CI_COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA
# Run your Python application when the container launches
CMD ["python", "-u", "main.py"]
