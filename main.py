import os
from prometheus_client import start_http_server, Gauge
import time
import random


# Get the PORT environment variable or use 8000 as the default
port = int(os.environ.get("PORT", 8000))
# Get the CI_COMMIT_SHORT_SHA environment variable or use an empty string as the default
git_commit = str(os.environ.get("CI_COMMIT_SHORT_SHA", "unknown"))


# Create a Gauge metric to track a value
peers_metric = Gauge('node_peers', 'Description of gauge metric')
block_height = Gauge('node_block_height', 'Application uptime in seconds')
commit_hash = Gauge('node_release_git_hash', 'Current release commit hash',("version",))

# Record the application start time
start_time = time.time()

# Function to update the gauge metric with a random value
def update_gauge_metric():
    value = random.randint(0, 100)
    peers_metric.set(value)

if __name__ == '__main__':
    # Start the Prometheus HTTP server
    start_http_server(port)
    print(f"Started prometheus server on port {port}")
    print(f"Version: {git_commit}")
    commit_hash.labels(version=git_commit).set(1)
   
    while True:
        print('Refreshing metrics...')
        # Update the gauge metric with a random value
        update_gauge_metric()
   
        # Calculate and update the uptime metric
        uptime = time.time() - start_time
        block_height.set(uptime)


        # Sleep for a few seconds (simulating some work)
        print('Done. Sleeping for 5 seconds.')
        time.sleep(5)
